# Crownstack code challange

# Steps to run project

1. Download or clone file in to php localhost

2. Import DB from DB folder in root of the project.

3. change the database configurations in .env file\

4. Go to the project root with terminal

5. Run php artisan serve command

6. Project will run on 8000 port of localhost server


# Discount API URL

http://localhost:8000/api/discounts

# Note:-
Please feel free to contact on aaaatulmishra@gnail.com for any further assistance